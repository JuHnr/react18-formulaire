import { useState, useEffect } from 'react'

import './App.css'

function App() {
  
  function Form() {
    //on crée les states pour chaque variable
    const [nameValue, setNameValue] = useState("");
    const [firstNameValue, setFirstNameValue] = useState("");
    const [ageValue, setAgeValue] = useState("");
    const [mailValue, setMailValue] = useState("");
    const [passwordValue, setPasswordValue] = useState("");

    //state pour le message d'erreur
    const [errorMessage, setErrorMessage] = useState("");

    //fonction qui s'exécute à la soumission du formulaire
    const handleSubmit = (event) => {
      //empêche le râfraichissement de la page
      event.preventDefault();

      //Affiche le message d'erreur selon l'âge à la soumission
      if (!validateAge(ageValue)) {
        setErrorMessage("Vous devez avoir + de 18ans");
      } else {
        setErrorMessage("");
      }

      //affiche les infos recueillies par le formulaire
      console.log(`Nom : ${nameValue}`);
      console.log(`Prénom : ${firstNameValue}`);
      console.log(`Age : ${ageValue}`);
      console.log(`Email : ${mailValue}`);
      console.log(`Mot de passe : ${passwordValue}`);
    };

    //validation de l'âge (retourne true ou false)
    function validateAge(input) {
      return input >= 18;
    }


    /*
    Si l'on voulait que le message d'erreur s'affiche dès l'arrivée sur le formulaire => utilisation de useEffect()
    Le message s'afficherait ou non selon l'input de l'utilisateur (en direct et non à la soumission) :

    useEffect(() => {
      if (!validateAge(ageValue)) {
        setErrorMessage("Vous devez avoir + de 18ans");
      } else {
        setErrorMessage("");
      }
    }, [ageValue]);
    
    */
    
    //Formulaire du composant 

    return (

      // à la soumission du formulaire, exécute handleSubmit()
      <form onSubmit={handleSubmit}>
        <ul>
          <li>
            <label htmlFor="name">Nom</label>
            <input
              type="text"
              value={nameValue} //récupère la valeur stockée dans nameValue
              onChange={(e) => setNameValue(e.target.value)} //Récupère la valeur de l'input sur lequel l'event onchange a lieu. Modifie et stocke cette valeur de nameValue grâce à setNameValue. 
              id="name"
              name="name"
            />
          </li>
          <li>
            <label htmlFor="firstName">Prénom</label>
            <input
              type="text"
              value={firstNameValue}
              onChange={(e) => setFirstNameValue(e.target.value)}
              id="firstName"
              name="firstName"
            />
          </li>
          <li>
            <label htmlFor="age">Âge</label>
            <input
              type="number"
              value={ageValue}
              onChange={(e) => setAgeValue(e.target.value)}
              id="age"
              name="age"
            />
          </li>
          <li>
            <label htmlFor="email">E-mail</label>
            <input
              type="email"
              value={mailValue}
              onChange={(e) => setMailValue(e.target.value)}
              id="email"
              name="email"
            />
          </li>
          <li>
            <label htmlFor="password">Mot de passe</label>
            <input
              type="password"
              value={passwordValue}
              onChange={(e) => setPasswordValue(e.target.value)}
              id="password"
              name="password"
            />
          </li>
        </ul>

        {/* affiche le message d'erreur dans une div si errorMessage n'est pas falsy (vide) */}
        {errorMessage && <div>{errorMessage}</div>}

        <button type="submit">Envoyer</button>
      </form>
    );
  }

  return (
    <>
      <Form/>
    </>
  )
}

export default App
